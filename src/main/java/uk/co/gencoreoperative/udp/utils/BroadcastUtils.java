package uk.co.gencoreoperative.udp.utils;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Address utilities specifically for broadcasting.
 *
 * @author Robert Wapshott
 */
public class BroadcastUtils {
    /**
     * @return An InetAddress that is set to broadcast on the local subnet.
     */
    public InetAddress convertToBroadcast(InetAddress address) {
        byte[] parts = address.getAddress();
        if (parts.length != 4) {
            throw new IllegalStateException("Invalid address: " + address.getHostAddress());
        }

        // Set the last part of the address to be multicast
        parts[3] = (byte) 255;

        try {
            return InetAddress.getByAddress(parts);
        } catch (UnknownHostException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Note: This call will filter out IP6 addresses.
     * @return An array of local addresses. Could possibly be null.
     * @throws IllegalStateException If there was a problem resolving the localhost.
     */
    public Collection<InetAddress> getLocalAddresses() {
        try {
            InetAddress localhost = InetAddress.getLocalHost();
            String name = localhost.getCanonicalHostName();
            Collection<InetAddress> result = Arrays.asList(InetAddress.getAllByName(name));

            return filterIP6Addresses(result);
        } catch (UnknownHostException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Remove any IP6 Addresses from the collection of addresses.
     * @param addresses Addresses to filter.
     * @return A collection which has been filtered.
     */
    public Collection<InetAddress> filterIP6Addresses(Collection<InetAddress> addresses) {
        List<InetAddress> result = new LinkedList<InetAddress>();
        for (InetAddress address : addresses) {
            if (address instanceof Inet6Address) {
                continue;
            }
            result.add(address);
        }
        return result;
    }
}
