package uk.co.gencoreoperative.udp.utils;

import uk.co.gencoreoperative.udp.UDPConstants;

import java.net.DatagramPacket;

/**
 * A collection of utility functions for use when working with packets.
 *
 * @author Robert Wapshott
 */
public class PacketUtils {
    /**
     * @return Generates a UDP packet based on the UDPConstants packet size.
     */
    public DatagramPacket generatePacket() {
        byte[] data = new byte[UDPConstants.PACKET_SIZE];
        DatagramPacket packet = new DatagramPacket(data, data.length);
        return packet;
    }


    /**
     * @param data The data to populate in the packet.
     * @return A prepopulated packet with the data provided.
     */
    public DatagramPacket generatePacket(byte[] data) {
        DatagramPacket packet = generatePacket();
        System.arraycopy(data, 0, packet.getData(), 0, data.length);
        packet.setLength(data.length);
        return packet;
    }

    /**
     * @param port Validate that the port is within the valid range.
     */
    public void validatePort(int port) {
        if (port < 0 || port >= UDPConstants.MAX_PORT) {
            throw new IllegalArgumentException("Invalid port number: " + port);
        }
    }
}
