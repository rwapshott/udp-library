package uk.co.gencoreoperative.udp.utils;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Determines the available IP Addresses of the system.
 *
 * @author Robert Wapshott
 */
public class AddressUtils {
    /**
     * Note: This call will filter out IP6 addresses.
     * @return Returns a collection of the local address in HostnameEntry format.
     */
    public Collection<InetAddress> geLocalAddresses() {
        List<InetAddress> entries = new LinkedList<InetAddress>();
        for (InetAddress address : getLocalAddresses()) {
            if (address instanceof Inet6Address) {
                continue;
            }

            entries.add(address);
        }
        return entries;
    }

    /**
     * Note: This call will filter out IP6 addresses.
     * @return An array of local addresses. Could possibly be null.
     * @throws IllegalStateException If there was a problem resolving the localhost.
     */
    public Collection<InetAddress> getLocalAddresses() {
        try {
            InetAddress localhost = InetAddress.getLocalHost();
            String name = localhost.getCanonicalHostName();
            Collection<InetAddress> result = Arrays.asList(InetAddress.getAllByName(name));

            return filterIP6Addresses(result);
        } catch (UnknownHostException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Remove any IP6 Addresses from the collection of addresses.
     * @param addresses Addresses to filter.
     * @return A collection which has been filtered.
     */
    public Collection<InetAddress> filterIP6Addresses(Collection<InetAddress> addresses) {
        List<InetAddress> result = new LinkedList<InetAddress>();
        for (InetAddress address : addresses) {
            if (address instanceof Inet6Address) {
                continue;
            }
            result.add(address);
        }
        return result;
    }
}
