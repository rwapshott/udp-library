package uk.co.gencoreoperative.udp;

/**
 * A collection of shared constants for the UDP implementation.
 *
 * @author Robert Wapshott
 */
public class UDPConstants {
    /**
     * All messages passed between peers will have packets up to this size.
     * Arbitrarily chosen, could be larger or smaller.
     */
    public static final int PACKET_SIZE = 1500;
    /**
     * The maximum possible port number in IP4.
     */
    public static final int MAX_PORT = 65535;
}
