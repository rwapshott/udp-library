package uk.co.gencoreoperative.udp.receive;

import uk.co.gencoreoperative.udp.utils.PacketUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

/**
 * Responsible for listening to packets received from the network.
 *
 * This class wraps around the details of UDP networking and presents a simpler interface
 * for code wishing to perform a simple networking task.
 *
 * @author Robert Wapshott
 */
public class Listen implements Runnable {
    private PacketReceived receiver;
    private PacketUtils packetUtils;
    private DatagramSocket socket;
    private int port = -1;

    /**
     * Create an instance of Listen.
     * @param receiver The receiver to process messages received.
     */
    public Listen(PacketReceived receiver) {
        this(new PacketUtils(), receiver);
    }

    /**
     * Dependency Injection constructor.
     * @param packetUtils Required for packet operations.
     * @param receiver Reciever to process messages.
     */
    public Listen(PacketUtils packetUtils, PacketReceived receiver) {
        this.receiver = receiver;
        this.packetUtils = packetUtils;
    }

    /**
     * Define a port number to listen to. Only packets sent to this port number will be
     * processed.
     *
     * @param port Positive integer.
     *
     * @return This instance.
     */
    public Listen onPort(int port) {
        packetUtils.validatePort(port);
        this.port = port;
        return this;
    }

    /**
     * Start a background thread to listen for packets and trigger the PacketReceived
     * implementation when one arrives.
     */
    public void start() {
        if (port == -1) {
            throw new IllegalStateException("port number must be assigned");
        }

        final Thread thread = new Thread(this);
        thread.setDaemon(true);
        thread.start();
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                thread.interrupt();
            }
        }));
    }

    @Override
    public void run() {
        // Initialise the Socket
        try {
            socket = new DatagramSocket(port);
            socket.setSoTimeout(500);
        } catch (SocketException e) {
            throw new IllegalStateException(e);
        }

        // Listen for packets.
        while (!Thread.interrupted()) {
            DatagramPacket packet = packetUtils.generatePacket();

            try {
                socket.receive(packet);
            } catch (SocketTimeoutException e) {
                // This is ok, and allows the Runnable to be more responsive to interrupts.
                continue;
            } catch (IOException e) {
                return;
            }

            // Process the message contents.
            byte[] contents = new byte[packet.getLength()];
            System.arraycopy(packet.getData(), 0, contents, 0, packet.getLength());
            receiver.received(contents, packet.getAddress(), packet.getPort());

        }

        socket.close();
        socket = null;
    }
}
