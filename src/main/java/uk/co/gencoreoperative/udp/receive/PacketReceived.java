package uk.co.gencoreoperative.udp.receive;

import java.net.InetAddress;

/**
 * Describes the ability to receive a packet of data and process it in an appropriate way.
 *
 * @author Robert Wapshott
 */
public interface PacketReceived {
    /**
     * A packet of data has been received.
     *
     * @param data A byte buffer of data.
     * @param address The address of where the packet was send from.
     * @param port The port the packet was sent from.
     */
    void received(byte[] data, InetAddress address, int port);
}
