package uk.co.gencoreoperative.udp.broadcast;

import uk.co.gencoreoperative.udp.utils.PacketUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * Responsible for sending a message to a remote location.
 *
 * @author Robert Wapshott
 */
public class Send {
    private DatagramSocket socket;
    private PacketUtils packetUtils;

    /**
     * Generate an instance of Send.
     */
    public Send() {
        this(new PacketUtils());
    }

    /**
     * Dependency Injection constructor.
     * @param packetUtils Required.
     */
    public Send(PacketUtils packetUtils) {
        this.packetUtils = packetUtils;
    }

    /**
     * @return A non null DatagramSocket.
     */
    private DatagramSocket getSocket() {
        if (socket == null) {
            try {
                socket = new DatagramSocket();
            } catch (SocketException e) {
                throw new IllegalStateException(e);
            }
        }
        return socket;
    }

    /**
     * Sent a message to another system on the network.
     *
     * @param address Required to know where to send the message to.
     * @param port Required as above.
     * @param data The message to send.
     */
    public void message(InetAddress address, int port, byte[] data) {
        if (address == null || port == -1) {
            throw new IllegalArgumentException("Address and Port must be provided.");
        }

        // Prepare the packet for sending.
        DatagramPacket packet = packetUtils.generatePacket(data);

        // Address it
        packet.setAddress(address);
        packet.setPort(port);

        // Send it
        try {
            getSocket().send(packet);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
