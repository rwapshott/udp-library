package uk.co.gencoreoperative.udp.broadcast;

import uk.co.gencoreoperative.udp.utils.BroadcastUtils;
import uk.co.gencoreoperative.udp.utils.PacketUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * Represents the ability to broadcast a packet across all network adapters to a given port number.
 *
 * This class wraps around the details of UDP networking.
 *
 * Note: Unlike TCP sockets where the socket can only be connected to one location, UDP packets are addressed,
 * thus it does not matter which port the packet is sent from only the address it is sent to. In this case
 * the packet is addressed to all locations on the same subnet.
 *
 * @author Robert Wapshott
 */
public class Broadcast {
    private final BroadcastUtils broadcastUtils;
    private DatagramSocket socket;
    private PacketUtils packetUtils;

    /**
     * Default constructor creates an instance of Broadcast.
     */
    public Broadcast() {
        this(new BroadcastUtils(), new PacketUtils());
    }

    /**
     * Dependency Injection constructor for creating an instance of Broadcast.
     * @param broadcastUtils Required.
     * @param packetUtils
     */
    public Broadcast(BroadcastUtils broadcastUtils, PacketUtils packetUtils) {
        this.broadcastUtils = broadcastUtils;
        this.packetUtils = packetUtils;
    }

    /**
     * Allows reuse of a socket to save churn on port numbers.
     * @return Non null socket.
     */
    private DatagramSocket getSocket() {
        if (socket == null) {
            try {
                socket = new DatagramSocket();
            } catch (SocketException e) {
                throw new IllegalStateException(e);
            }
        }
        return socket;
    }

    /**
     * Broadcasts the given packet to all network adapters.
     *
     * The packet will be broadcast using the assigned port.
     *
     * @param port The port to send the packet to. Required.
     * @param data data to send in a packet via a broadcast.
     *
     * @return This instance.
     */
    public Broadcast send(int port, byte[] data) {
        if (port == -1) {
            throw new IllegalStateException("Port must be assigned");
        }

        try {
            // Prepare the packet
            DatagramPacket packet = packetUtils.generatePacket(data);

            for (InetAddress address : broadcastUtils.getLocalAddresses()) {

                // Convert the local address to a broadcast address
                InetAddress broadcast = broadcastUtils.convertToBroadcast(address);
                packet.setAddress(broadcast);
                packet.setPort(port);
                getSocket().send(packet);
            }

        } catch (SocketException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return this;
    }
}
