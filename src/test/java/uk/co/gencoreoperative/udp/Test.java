package uk.co.gencoreoperative.udp;

import uk.co.gencoreoperative.udp.broadcast.Broadcast;
import uk.co.gencoreoperative.udp.broadcast.Send;
import uk.co.gencoreoperative.udp.receive.Listen;
import uk.co.gencoreoperative.udp.receive.PacketReceived;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.MessageFormat;

/**
 * Test will start up both Listen and Broadcast elements of the library and demonstrate that they can interoperate
 * correctly. This is also a test of how simple the library is to implement.
 *
 * @author Robert Wapshott
 */
public class Test {
    public static void main(String[] args) {
        int port = 12346;

        PacketReceived receiver = new PacketReceived() {
            @Override
            public void received(byte[] data, InetAddress address, int port) {
                String output = MessageFormat.format(
                        "Received from: {0}:{1}\n{2}",
                        address.getHostAddress(),
                        Integer.toString(port),
                        new String(data));
                System.out.println(output);
            }
        };

        new Listen(receiver).onPort(port).start();

        byte[] data = "Hello World".getBytes();
        new Broadcast().send(port, data);

        try {
            new Send().message(InetAddress.getLocalHost(), port, data);
        } catch (UnknownHostException e) {
            throw new IllegalStateException(e);
        }

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }
    }
}
